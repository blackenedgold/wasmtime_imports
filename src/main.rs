use wasmtime::{Linker, Module, Store};

fn main() {
    let store = Store::default();
    // linkerを準備し、
    let mut linker = Linker::new(&store);
    // 関数を登録する
    linker
        .func("ffi", "print", |x: i32| println!("{}", x))
        .expect("function registration failed");
    let module = Module::new(
        store.engine(),
        // "ffi"モジュールの"print"をimportして使う
        r#"
(module
  (func $print (import "ffi" "print") (param i32))
  (func $add (param i32 i32) (result i32)
    (return (i32.add (get_local 0) (get_local 1))))
  (func $main
    (call $print (call $add (i32.const 1) (i32.const 2))))
  (start $main)
)"#,
    )
    .expect("failed to create module");
    // linkerを使ってインスタンス化する
    let _ = linker
        .instantiate(&module)
        .expect("failed to instantiate module");
}
